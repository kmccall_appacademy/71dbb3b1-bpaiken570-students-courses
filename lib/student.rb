
class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    unless @courses.include?(course)
      raise if @courses.any? {|el| el.conflicts_with?(course) }
      @courses.push(course)
      course.students << self
    end
  end

  def course_load
    hash = {}
    @courses.each do |course|
      hash[course.department] = 0 unless hash.keys.include?(course.department)
      hash[course.department] += course.credits
    end
    hash
  end
end
